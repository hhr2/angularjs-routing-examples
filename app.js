angular
    .module('ordersapp', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
.state('orders', {
    abstract: true,
    url: '/orders',
    views: {
        header: {
            templateUrl: 'components/templates/common/header.html'
        },
        content: {
            templateUrl: 'components/templates/common/content.html'
        },
        footer: {
            templateUrl: 'components/templates/common/footer.html'
        }
    }
})
            .state('orders.list', {
                url: '/list',
                controller: 'OrdersCtrl',
                templateUrl: 'orders/tpl/list.html'
            })
            .state('orders.new', {
                url: '/new',
                templateUrl: 'new-order/tpl/new.html',
                controller: 'NewOrderCtrl'
            })
            .state('orders.edit', {
                url: '/edit/:idx',
                templateUrl: 'view-order/tpl/view.html',
                controller: 'ViewOrderCtrl'
            })

        $urlRouterProvider.otherwise('/orders/list');
    });